﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using MihaZupan;
using Telegram.Bot;
using TelegramBot.Services;

namespace TelegramBot {
  internal class AppConfiguration {
    public static IConfiguration Configuration { get; set; }

    public static void Initialize() {
      try {
        var builder = new ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("settings.json");

        Configuration = builder.Build();

        //PrintConfiguration();
      }
      catch (Exception e) {
        Console.WriteLine("Error building configuration");
        Console.WriteLine(e);
      }
    }

    private static void PrintConfiguration() {
      Console.WriteLine("TelegramBot:");
      Console.WriteLine($"Token: {Configuration["TelegramBot:Token"]}");

      Console.WriteLine("WebService:");
      Console.WriteLine($"Address: {Configuration["WebService:Address"]}");
      Console.WriteLine("Authorization:");
      Console.WriteLine($"Enabled: {Configuration["WebService:Authorization:Enabled"]}");
      if (Configuration["WebService:Authorization:Enabled"] == "True") {
        Console.WriteLine($"Login: {Configuration["WebService:Authorization:Login"]}");
        Console.WriteLine($"Password: {Configuration["WebService:Authorization:Password"]}");
      }

      Console.WriteLine("Socks5Proxy:");
      Console.WriteLine($"Enabled: {Configuration["Socks5Proxy:Enabled"]}");
      if (Configuration["Socks5Proxy:Enabled"] == "True") {
        Console.WriteLine($"Address: {Configuration["Socks5Proxy:Address"]}");
        Console.WriteLine($"Port: {Configuration["Socks5Proxy:Port"]}");
      }
    }

    public static ITelegramBotClient GetBot() {
      if (Configuration["Socks5Proxy:Enabled"] != "True") {
        return new TelegramBotClient(Configuration["TelegramBot:Token"]);
      }
      var address = Configuration["Socks5Proxy:Address"];
      var port = int.Parse(Configuration["Socks5Proxy:Port"]);
      var proxy = new HttpToSocks5Proxy(address, port);
      return new TelegramBotClient(Configuration["TelegramBot:Token"], proxy);
    }

    public static void InitializeService(TrekerService service) {
      service.Address = Configuration["WebService:Address"];

      if (Configuration["WebService:Authorization:Enabled"] == "True") {
        service.Login = Configuration["WebService:Authorization:Login"];
        service.Password = Configuration["WebService:Authorization:Password"];
        service.IsUsingAuth = true;
      }
    }
  }
}