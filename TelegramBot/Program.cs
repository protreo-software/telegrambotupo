﻿using System.Threading;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using TelegramBot.Commands;
using TelegramBot.Services;

namespace TelegramBot {
  internal class Program {
    private static ITelegramBotClient _bot;
    private static object _locker = new object();

    private static void Main() {
      AppConfiguration.Initialize();
      _bot = AppConfiguration.GetBot();
      TrekerService.Instance.Initialize();

      _bot.OnMessage += BotOnMessageReceived;

      _bot.StartReceiving();
      Thread.Sleep(Timeout.Infinite);
      _bot.StopReceiving();
    }


    private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs) {
      lock (_locker) {
        var message = messageEventArgs.Message;

        if (message == null || message.Type != MessageType.Text)
        {
          return;
        }

        var userKey = message.From.Id;
        var command = CommandsRouter.SetCommand(message, userKey);

        command.Process(_bot, message);
      }
    }
  }
}