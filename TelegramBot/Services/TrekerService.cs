﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using TelegramBot.Commands.CheckCommand;
using TelegramBot.Commands.SSCommand;
using TelegramBot.Commands.TaskCommand;

namespace TelegramBot.Services {
  public sealed class TrekerService {
    private TrekerService() { }
    public string Address { get; set; }
    public string Login { get; set; }
    public string Password { get; set; }
    public bool IsUsingAuth { get; set; }

    public static TrekerService Instance => Nested.instance;

    public void Initialize() {
      AppConfiguration.InitializeService(this);
    }

    public async Task<SSCommandResponse> StartStopWork(SSCommandRequest request) {
      var envelope = StartStopWorkEnvelope(request);
      var response = await CallWebService(envelope, "StartStopWork");
      var result = ProcessSSEnvelopeResponse(response);

      return result;
    }

    private SSCommandResponse ProcessSSEnvelopeResponse(XmlDocument document) {
      var root = document.DocumentElement["soap:Body"]["m:StartStopWorkResponse"]["m:return"];
      var status = root["m:Status"]?.InnerText;
      var text = root["m:Text"]?.InnerText;
      var errorDescription = root["m:ErrorDescription"]?.InnerText;

      var response = new SSCommandResponse {Status = status, Text = text, ErrorDescription = errorDescription};

      return response;
    }

    public async Task<TaskCommandResponse> FillTask(TaskCommandRequest request) {
      var envelope = FillTaskEnvelope(request);
      var response = await CallWebService(envelope, "FillTask");
      var result = ProcessTaskEnvelopeResponse(response);

      return result;
    }

    private TaskCommandResponse ProcessTaskEnvelopeResponse(XmlDocument document) {
      var root = document.DocumentElement["soap:Body"]["m:FillTaskResponse"]["m:return"];
      var status = root["m:Status"]?.InnerText;
      var text = root["m:Text"]?.InnerText;
      var errorDescription = root["m:ErrorDescription"]?.InnerText;
      var tasks = root["m:Tasks"];
      var tasksList = new List<TaskItem>();

      if (tasks != null) {
        foreach (XmlElement task in tasks.ChildNodes) {
          var guid = task["m:GUID"].InnerText;
          var name = task["m:Name"].InnerText;
          var taskItem = new TaskItem {GUID = guid, Name = name};
          tasksList.Add(taskItem);
        }
      }

      var response = new TaskCommandResponse {
        Status = status,
        Text = text,
        ErrorDescription = errorDescription,
        Tasks = tasksList
      };

      return response;
    }

    public async Task<CheckCommandResponse> CheckUser(CheckCommandRequest request) {
      var envelope = CheckUserEnvelope(request);
      var response = await CallWebService(envelope, "CheckUser");
      var result = ProcessCheckEnvelopeResponse(response);

      return result;
    }

    private CheckCommandResponse ProcessCheckEnvelopeResponse(XmlDocument document) {
      var root = document.DocumentElement["soap:Body"]["m:CheckUserResponse"]["m:return"];
      var name = root["m:Name"]?.InnerText;
      var key = root["m:Key"]?.InnerText;
      var accessGranted = root["m:AccessGranted"]?.InnerText == "true";

      var response = new CheckCommandResponse {Name = name, Key = key, AccessGranted = accessGranted};

      return response;
    }

    private async Task<XmlDocument> CallWebService(XmlDocument soapEnvelope, string actionName) {
      var response = await Task.Run(async () => {
        var actionAddress = $"{Address}?op={actionName}";
        var webRequest = CreateWebRequest(Address, actionAddress);

        InsertSoapEnvelopeIntoWebRequest(soapEnvelope, webRequest);

        if (IsUsingAuth) {
          SetBasicAuthHeader(webRequest);
        }

        try {
          var webResponse = await webRequest.GetResponseAsync();
          var responseStream = webResponse.GetResponseStream();

          var xml = new XmlDocument();
          xml.LoadXml(new StreamReader(responseStream ?? throw new InvalidOperationException()).ReadToEnd());

          return xml;
        }
        catch (Exception e) {
          Console.WriteLine($"{e}\n\nProblem with connection to web-service {actionAddress}");
          Console.WriteLine(e);

          return new XmlDocument();
        }
      });

      return response;
    }

    private void SetBasicAuthHeader(WebRequest request) {
      var encoded = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{Login}:{Password}"));
      var authHeader = $"Basic {encoded}";
      request.Headers.Add(HttpRequestHeader.Authorization, authHeader);
    }

    private HttpWebRequest CreateWebRequest(string url, string action) {
      var webRequest = (HttpWebRequest) WebRequest.Create(url);
      webRequest.Headers.Add("SOAPAction", action);
      webRequest.ContentType = "text/xml;charset=\"utf-8\"";
      webRequest.Accept = "text/xml";
      webRequest.Method = "POST";
      return webRequest;
    }

    private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, WebRequest webRequest) {
      using (var stream = webRequest.GetRequestStream()) {
        soapEnvelopeXml.Save(stream);
      }
    }

    private static XmlDocument StartStopWorkEnvelope(SSCommandRequest request) {
      var soapEvelopeDocument = new XmlDocument();
      soapEvelopeDocument.LoadXml($@"
      <soap:Envelope 
        xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" 
        xmlns:tel=""http://www.space1s.ru/telegrambot"">
         <soap:Header/>
         <soap:Body>
            <tel:StartStopWork>
               <tel:UserKey>{request.UserKey}</tel:UserKey>
               <tel:Comment>{request.Comment}</tel:Comment>
               <tel:Duration>{request.Duration}</tel:Duration>
            </tel:StartStopWork>
         </soap:Body>
      </soap:Envelope>
      ");

      return soapEvelopeDocument;
    }

    private static XmlDocument CheckUserEnvelope(CheckCommandRequest request) {
      var soapEvelopeDocument = new XmlDocument();
      soapEvelopeDocument.LoadXml($@"
      <soap:Envelope 
        xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" 
        xmlns:tel=""http://www.space1s.ru/telegrambot"">
         <soap:Header/>
         <soap:Body>
            <tel:CheckUser>
               <tel:SearchString>{request.UserKey}</tel:SearchString>
            </tel:CheckUser>
         </soap:Body>
      </soap:Envelope>
      ");

      return soapEvelopeDocument;
    }

    private static XmlDocument FillTaskEnvelope(TaskCommandRequest request) {
      var soapEvelopeDocument = new XmlDocument();
      soapEvelopeDocument.LoadXml($@"
      <soap:Envelope 
        xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" 
        xmlns:tel=""http://www.space1s.ru/telegrambot"">
         <soap:Header/>
         <soap:Body>
            <tel:FillTask>
               <tel:UserKey>{request.UserKey}</tel:UserKey>
               <tel:TaskId>{request.TaskId}</tel:TaskId>
               <tel:Duration>{request.Duration}</tel:Duration>
               <tel:Comment>{request.Comment}</tel:Comment>
            </tel:FillTask>
         </soap:Body>
      </soap:Envelope>
      ");

      return soapEvelopeDocument;
    }

    private class Nested {
      internal static readonly TrekerService instance = new TrekerService();

      static Nested() { }
    }
  }
}