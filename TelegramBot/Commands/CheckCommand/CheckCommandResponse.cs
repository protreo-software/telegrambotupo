﻿namespace TelegramBot.Commands.CheckCommand {
  public class CheckCommandResponse {
    public string Name { get; set; }
    public string Key { get; set; }
    public bool AccessGranted { get; set; }
  }
}