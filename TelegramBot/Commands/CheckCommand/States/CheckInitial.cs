﻿using System.Threading.Tasks;
using TelegramBot.Services;

namespace TelegramBot.Commands.CheckCommand.States {
  internal class CheckInitial : CommandState {
    public override async Task Handle(Command command) {
      var userKey = command.UserMessage.From.Id.ToString();

      var commandRequest = new CheckCommandRequest {
        UserKey = userKey
      };

      var service = TrekerService.Instance;
      var result = await service.CheckUser(commandRequest);
      var responseText = $"Имя: {result.Name}\n" +
                         $"Ключ: {result.Key}\n" +
                         $"Доступ разрешен: {(result.AccessGranted ? "Да" : "Нет")}";

      await command.Bot.SendTextMessageAsync(command.UserMessage.Chat.Id, responseText);

      command.Finished = true;
    }
  }
}