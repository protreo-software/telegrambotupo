﻿using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace TelegramBot.Commands.CheckCommand {
  internal class CheckCommand : Command {
    public CheckCommand(CommandState state) {
      State = state;
    }

    public override async Task Process(ITelegramBotClient bot, Message message) {
      Bot = bot;
      UserMessage = message;
      await State.Handle(this);
    }
  }
}