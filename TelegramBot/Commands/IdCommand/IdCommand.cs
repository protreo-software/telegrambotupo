﻿using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace TelegramBot.Commands.IdCommand {
  /// <summary>
  ///   Returns Telegram User Id
  /// </summary>
  internal class IdCommand : Command {
    public IdCommand(CommandState state) {
      State = state;
    }

    public override async Task Process(ITelegramBotClient bot, Message message) {
      Bot = bot;
      UserMessage = message;
      await State.Handle(this);
    }
  }
}