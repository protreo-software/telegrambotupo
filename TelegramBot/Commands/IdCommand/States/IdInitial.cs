﻿using System.Threading.Tasks;

namespace TelegramBot.Commands.IdCommand.States {
  internal class IdInitial : CommandState {
    public override async Task Handle(Command command) {
      var chatId = command.UserMessage.Chat.Id;
      var userKey = command.UserMessage.From.Id;

      await command.Bot.SendTextMessageAsync(chatId, userKey.ToString());

      command.Finished = true;
    }
  }
}