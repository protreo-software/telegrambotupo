﻿using System.Threading.Tasks;

namespace TelegramBot.Commands {
  public abstract class CommandState {
    public abstract Task Handle(Command command);
  }
}