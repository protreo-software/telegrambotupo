﻿using System.Threading.Tasks;
using TelegramBot.Services;

namespace TelegramBot.Commands.TaskCommand.States {
  internal class TaskQuestion : CommandState {
    public override async Task Handle(Command command) {
      var chatId = command.UserMessage.Chat.Id;
      var userKey = command.UserMessage.From.Id.ToString();

      var commandRequest = new TaskCommandRequest {
        UserKey = userKey,
        Comment = command.UserMessage.Text,
        Duration = ((TaskCommand) command).Duration,
        TaskId = ((TaskCommand) command).SelectedTask.GUID
      };

      var service = TrekerService.Instance;
      var result = await service.FillTask(commandRequest);

      if (result.Status == "OK") {
        await command.Bot.SendTextMessageAsync(chatId, result.Text);
      }
      else if (result.Status == "Error") {
        await command.Bot.SendTextMessageAsync(chatId, result.ErrorDescription);
      }
      else {
        await command.Bot.SendTextMessageAsync(chatId, "Unknown Error");
      }

      command.Finished = true;
    }
  }
}