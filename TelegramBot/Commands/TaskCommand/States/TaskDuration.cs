﻿using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramBot.Commands.TaskCommand.States {
  internal class TaskDuration : CommandState {
    public override async Task Handle(Command command) {
      var chatId = command.UserMessage.Chat.Id;

      ((TaskCommand) command).Duration = command.UserMessage.Text;
      await command.Bot.SendTextMessageAsync(
        chatId, 
        "Укажите комментарий", 
        replyMarkup: new ForceReplyMarkup());

      command.State = new TaskQuestion();
    }
  }
}