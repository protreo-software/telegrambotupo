﻿using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramBot.Commands.TaskCommand.States {
  internal class TaskSelection : CommandState {
    public override async Task Handle(Command command) {
      var chatId = command.UserMessage.Chat.Id;

      ((TaskCommand) command).SelectedTask =
        ((TaskCommand) command).Tasks.First(t => t.Name == command.UserMessage.Text);

      await command.Bot.SendTextMessageAsync(
        chatId, 
        "Укажите время", 
        replyMarkup: new ForceReplyMarkup());

      command.State = new TaskDuration();
    }
  }
}