﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot.Services;

namespace TelegramBot.Commands.TaskCommand.States
{
  internal class TaskInitial : CommandState
  {
    public override async Task Handle(Command command)
    {
      var chatId = command.UserMessage.Chat.Id;
      var userKey = command.UserMessage.From.Id.ToString();

      var commandRequest = new TaskCommandRequest()
      {
        UserKey = userKey,
        Comment = "",
        Duration = "",
        TaskId = ""
      };

      var service = TrekerService.Instance;
      var result = await service.FillTask(commandRequest);

      if (result.Status == "OK")
      {
        await command.Bot.SendTextMessageAsync(chatId, result.Text);

        command.Finished = true;
      }
      else if (result.Status == "TaskSelection") {
        ((TaskCommand) command).Tasks = result.Tasks;

        var rows = result.Tasks
          .Select(task => new KeyboardButton(task.Name))
          .Select(button => new List<KeyboardButton> {button}).ToList();
        var keyboard = new ReplyKeyboardMarkup(rows, true, true);

        await command.Bot.SendTextMessageAsync(chatId, "Выберите задание", replyMarkup: keyboard);

        command.State = new TaskSelection();
      }
      else if (result.Status == "Error")
      {
        await command.Bot.SendTextMessageAsync(chatId, result.ErrorDescription);

        command.Finished = true;
      }
      else
      {
        await command.Bot.SendTextMessageAsync(
          chatId,
          "Unknown Error");

        command.Finished = true;
      }
    }
  }
}