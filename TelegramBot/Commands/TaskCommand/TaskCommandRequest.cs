﻿namespace TelegramBot.Commands.TaskCommand {
  public class TaskCommandRequest {
    public string UserKey { get; set; }
    public string TaskId { get; set; }
    public string Duration { get; set; }
    public string Comment { get; set; }
  }
}