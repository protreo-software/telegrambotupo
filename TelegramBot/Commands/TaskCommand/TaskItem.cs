﻿namespace TelegramBot.Commands.TaskCommand {
  public class TaskItem {
    public string GUID { get; set; }
    public string Name { get; set; }
  }
}