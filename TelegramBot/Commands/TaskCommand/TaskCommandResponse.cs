﻿using System.Collections.Generic;

namespace TelegramBot.Commands.TaskCommand {
  public class TaskCommandResponse {
    public string Status { get; set; }
    public string Text { get; set; }
    public string ErrorDescription { get; set; }
    public List<TaskItem> Tasks { get; set; }
  }
}