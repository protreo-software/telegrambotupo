﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace TelegramBot.Commands.TaskCommand {
  internal class TaskCommand : Command {
    public TaskCommand(CommandState state) {
      State = state;
    }

    public List<TaskItem> Tasks { get; set; }
    public string Duration { get; set; }
    public TaskItem SelectedTask { get; set; }

    public override async Task Process(ITelegramBotClient bot, Message message) {
      Bot = bot;
      UserMessage = message;
      await State.Handle(this);
    }
  }
}