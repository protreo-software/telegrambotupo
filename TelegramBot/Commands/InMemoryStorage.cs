﻿using System.Collections.Generic;

namespace TelegramBot.Commands {
  internal class InMemoryStorage {
    private static readonly Dictionary<int, Command> _commands = new Dictionary<int, Command>();

    public static bool RemoveCommand(int userKey) {
      return _commands.Remove(userKey);
    }

    public static void AddCommand(int userKey, Command command) {
      if (_commands.ContainsKey(userKey)) {
        _commands.Remove(userKey);
      }

      _commands.Add(userKey, command);
    }

    public static Command GetCommand(int userKey) {
      return _commands.GetValueOrDefault(userKey, null);
    }
  }
}