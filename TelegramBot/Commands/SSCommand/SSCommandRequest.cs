﻿namespace TelegramBot.Commands.SSCommand {
  public class SSCommandRequest {
    public string UserKey { get; set; }
    public string Comment { get; set; }
    public string Duration { get; set; }
  }
}