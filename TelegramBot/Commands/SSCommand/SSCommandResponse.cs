﻿namespace TelegramBot.Commands.SSCommand {
  public class SSCommandResponse {
    public string Status { get; set; }
    public string Text { get; set; }
    public string ErrorDescription { get; set; }
  }
}