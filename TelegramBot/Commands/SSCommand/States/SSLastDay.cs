﻿using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramBot.Commands.SSCommand.States {
  internal class SSLastDay : CommandState {
    public override async Task Handle(Command command) {
      var chatId = command.UserMessage.Chat.Id;

      ((SSCommand) command).Duration = command.UserMessage.Text;

      await command.Bot.SendTextMessageAsync(
        chatId, 
        "Введите примечание", 
        replyMarkup: new ForceReplyMarkup());

      command.State = new SSQuestion();
    }
  }
}