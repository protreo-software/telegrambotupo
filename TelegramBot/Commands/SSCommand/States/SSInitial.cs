﻿using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot.Services;

namespace TelegramBot.Commands.SSCommand.States {
  internal class SSInitial : CommandState {
    public override async Task Handle(Command command) {
      var chatId = command.UserMessage.Chat.Id;
      var userKey = command.UserMessage.From.Id.ToString();

      var commandRequest = new SSCommandRequest {
        UserKey = userKey,
        Comment = "",
        Duration = ""
      };

      var service = TrekerService.Instance;
      var result = await service.StartStopWork(commandRequest);

      if (result.Status == "OK") {
        await command.Bot.SendTextMessageAsync(chatId, result.Text);

        command.Finished = true;
      }
      else if (result.Status == "Question") {
        await command.Bot.SendTextMessageAsync(chatId, result.ErrorDescription, replyMarkup: new ForceReplyMarkup());

        command.State = new SSQuestion();
      }
      else if (result.Status == "LastDay") {
        await command.Bot.SendTextMessageAsync(chatId, result.ErrorDescription, replyMarkup: new ForceReplyMarkup());

        command.State = new SSLastDay();
      }
      else if (result.Status == "Error") {
        await command.Bot.SendTextMessageAsync(
          chatId,
          result.ErrorDescription);

        command.Finished = true;
      }
      else {
        await command.Bot.SendTextMessageAsync(
          chatId,
          "Unknown Error");

        command.Finished = true;
      }
    }
  }
}