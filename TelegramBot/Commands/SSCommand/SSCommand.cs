﻿using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace TelegramBot.Commands.SSCommand {
  /// <summary>
  ///   Logs in/Logs off user in UPO
  /// </summary>
  internal class SSCommand : Command {
    public SSCommand(CommandState state) {
      State = state;
    }

    public string Duration { get; set; }

    public override async Task Process(ITelegramBotClient bot, Message message) {
      Bot = bot;
      UserMessage = message;
      await State.Handle(this);
    }
  }
}