﻿using System.Threading.Tasks;

namespace TelegramBot.Commands.CancelCommand.States {
  internal class CancelInitial : CommandState {
    public override async Task Handle(Command command) {
      InMemoryStorage.RemoveCommand(command.UserMessage.From.Id);

      await command.Bot.SendTextMessageAsync(command.UserMessage.Chat.Id, "Диалог удален");

      command.Finished = true;
    }
  }
}