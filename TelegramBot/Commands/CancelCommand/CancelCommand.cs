﻿using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace TelegramBot.Commands.CancelCommand {
  internal class CancelCommand : Command {
    public CancelCommand(CommandState state) {
      State = state;
    }

    public override async Task Process(ITelegramBotClient bot, Message message) {
      Bot = bot;
      UserMessage = message;
      await State.Handle(this);
    }
  }
}