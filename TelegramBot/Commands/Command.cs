﻿using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace TelegramBot.Commands {
  public abstract class Command {
    public CommandState State { get; set; }
    public ITelegramBotClient Bot { get; set; }
    public Message UserMessage { get; set; }
    public bool Finished { get; set; }
    public abstract Task Process(ITelegramBotClient bot, Message message);
  }
}