﻿using System.Linq;
using Telegram.Bot.Types;
using TelegramBot.Commands.CancelCommand.States;
using TelegramBot.Commands.CheckCommand.States;
using TelegramBot.Commands.HelpCommand.States;
using TelegramBot.Commands.IdCommand.States;
using TelegramBot.Commands.SSCommand.States;
using TelegramBot.Commands.TaskCommand.States;

namespace TelegramBot.Commands {
  internal class CommandsRouter {
    public static Command SetCommand(Message message, int userKey) {
      Command command;
      switch (message.Text.Split(' ').First()) {
        case "/ss":
          command = SetSSCommand(userKey);
          break;
        case "/id":
          command = new IdCommand.IdCommand(new IdInitial());
          break;
        case "/task":
          command = SetTaskCommand(userKey);
          break;
        case "/check":
          command = new CheckCommand.CheckCommand(new CheckInitial());
          break;
        case "/cancel":
          command = new CancelCommand.CancelCommand(new CancelInitial());
          break;
        case "/help":
          command = new HelpCommand.HelpCommand(new HelpInitial());
          break;
        default:
          command = GetDefaultCommand(userKey);
          break;
      }

      return command;
    }

    private static Command GetDefaultCommand(int userKey) {
      var command = InMemoryStorage.GetCommand(userKey);
      command = DiscardCommandIfFinished(userKey, command);

      return command ?? new HelpCommand.HelpCommand(new HelpInitial());
    }

    private static Command SetTaskCommand(int userKey) {
      var command = GetSavedCommand(userKey);

      if (command != null) {
        return command;
      }

      command = new TaskCommand.TaskCommand(new TaskInitial());
      InMemoryStorage.AddCommand(userKey, command);

      return command;
    }

    private static Command GetSavedCommand(int userKey) {
      var command = InMemoryStorage.GetCommand(userKey);

      command = DiscardCommandIfFinished(userKey, command);

      return command;
    }

    private static Command SetSSCommand(int userKey) {
      var command = GetSavedCommand(userKey);

      if (command != null) {
        return command;
      }

      command = new SSCommand.SSCommand(new SSInitial());
      InMemoryStorage.AddCommand(userKey, command);

      return command;
    }

    private static Command DiscardCommandIfFinished(int userKey, Command command) {
      if (command == null || !command.Finished) {
        return command;
      }
      InMemoryStorage.RemoveCommand(userKey);
      return null;
    }
  }
}