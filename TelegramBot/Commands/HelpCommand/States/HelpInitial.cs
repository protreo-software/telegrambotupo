﻿using System.Threading.Tasks;

namespace TelegramBot.Commands.HelpCommand.States {
  internal class HelpInitial : CommandState {
    public override async Task Handle(Command command) {
      await command.Bot.SendTextMessageAsync(command.UserMessage.Chat.Id, "Not Implemented");
    }
  }
}